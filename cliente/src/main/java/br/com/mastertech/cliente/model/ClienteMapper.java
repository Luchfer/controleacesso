package br.com.mastertech.cliente.model;


import br.com.mastertech.cliente.request.ClienteRequest;
import br.com.mastertech.cliente.response.ClienteCreatedResponse;
import br.com.mastertech.cliente.response.ClienteDetailResponse;
import org.springframework.stereotype.Component;

@Component
public class ClienteMapper {

    public Cliente toCliente(ClienteRequest clienteRequest){
        Cliente cliente = new Cliente();
        cliente.setName(clienteRequest.getName());
        return cliente;
    }

    public ClienteCreatedResponse toClienteCreatedResponse(Cliente cliente) {
        ClienteCreatedResponse clienteCreatedResponse = new ClienteCreatedResponse();
        clienteCreatedResponse.setId(cliente.getId());
        clienteCreatedResponse.setName(cliente.getName());
        return clienteCreatedResponse;
    }

    public ClienteDetailResponse toClienteDetailResponse(Cliente cliente){
        ClienteDetailResponse clienteDetailResponse = new ClienteDetailResponse();
        clienteDetailResponse.setId(cliente.getId());
        clienteDetailResponse.setName(cliente.getName());
        return clienteDetailResponse;
    }


}
