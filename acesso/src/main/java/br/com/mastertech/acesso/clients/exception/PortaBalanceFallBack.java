package br.com.mastertech.acesso.clients.exception;

import br.com.mastertech.acesso.clients.PortaClient;
import br.com.mastertech.acesso.clients.dto.PortaDTO;
import com.netflix.client.ClientException;

public class PortaBalanceFallBack implements PortaClient {

    private Exception exception;

    public PortaBalanceFallBack(Exception exception) {
        this.exception = exception;
    }

    @Override
    public PortaDTO getPorta(long idPorta) {
        if(exception.getCause() instanceof ClientException){
            throw  new PortaNotFoundException();
        }
        throw (RuntimeException) exception;
    }
}
