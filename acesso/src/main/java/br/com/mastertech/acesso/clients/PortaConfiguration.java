package br.com.mastertech.acesso.clients;


import br.com.mastertech.acesso.clients.exception.PortaBalanceFallBack;
import feign.Feign;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;


public class PortaConfiguration {

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallbackFactory( PortaBalanceFallBack::new, RuntimeException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }

}
