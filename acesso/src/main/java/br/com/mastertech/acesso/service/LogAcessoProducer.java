package br.com.mastertech.acesso.service;

import br.com.mastertech.logacesso.model.LogAcesso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class LogAcessoProducer {

    @Autowired
    private KafkaTemplate<String, LogAcesso> producer;

    public void enviarAoKafka(LogAcesso logAcesso) {

        producer.send("spec4-fernando-luchiari-1", logAcesso);
        producer.send("spec4-fernando-luchiari-2", logAcesso);
        producer.send("spec4-fernando-luchiari-3", logAcesso);
       /*
       spec4-fernando-luchiari-1
        spec4-fernando-luchiari-2
        spec4-fernando-luchiari-3*/
    }

}