package br.com.mastertech.acesso.service;

import br.com.mastertech.acesso.clients.Client;
import br.com.mastertech.acesso.clients.PortaClient;
import br.com.mastertech.acesso.clients.dto.ClienteDTO;
import br.com.mastertech.acesso.clients.dto.PortaDTO;
import br.com.mastertech.acesso.clients.exception.ClienteNotFoundException;
import br.com.mastertech.acesso.clients.exception.PortaNotFoundException;
import br.com.mastertech.acesso.model.Acesso;
import br.com.mastertech.logacesso.model.LogAcesso;
import br.com.mastertech.acesso.repository.AcessoRepository;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AcessoService {
    @Autowired
    private AcessoRepository acessoRepository;
    @Autowired
    private Client client;
    @Autowired
    private PortaClient portaClient;
    @Autowired
    private LogAcessoProducer logAcessoProducer;

    public Acesso create(Acesso dadosAcesso){
        PortaDTO porta = null;
        ClienteDTO cliente = null;
        try{
            porta = portaClient.getPorta(dadosAcesso.getIdPorta());
        }catch (FeignException.FeignClientException.NotFound e){
            throw new PortaNotFoundException();
        }

        try{
            cliente = client.getCliente(dadosAcesso.getIdCliente());
        }catch (FeignException.FeignClientException.NotFound e){
            throw new ClienteNotFoundException();
        }
        dadosAcesso = acessoRepository.save(dadosAcesso);
        return dadosAcesso;
    }

    public Acesso getAcesso(long idCliente,long idPorta){
        Acesso dadosAcesso = acessoRepository.findByIdPortaAndIdCliente(idPorta,idCliente);
        LogAcesso logAcesso = new LogAcesso();
        logAcesso.setIdCliente(idCliente);
        logAcesso.setIdPorta(idPorta);
        if(dadosAcesso !=null){
            logAcesso.setPossuiAcesso(Boolean.TRUE);
        }else{
            logAcesso.setPossuiAcesso(Boolean.FALSE);
        }
        logAcessoProducer.enviarAoKafka(logAcesso);
        return dadosAcesso;
    }

}
