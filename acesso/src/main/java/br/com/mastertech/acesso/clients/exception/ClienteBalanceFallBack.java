package br.com.mastertech.acesso.clients.exception;

import br.com.mastertech.acesso.clients.Client;
import br.com.mastertech.acesso.clients.dto.ClienteDTO;
import com.netflix.client.ClientException;

public class ClienteBalanceFallBack implements Client {
    private Exception exception;

    public ClienteBalanceFallBack(Exception exception) {
        this.exception = exception;
    }

    @Override
    public ClienteDTO getCliente(long idCliente) {
        if(exception.getCause() instanceof ClientException){
            throw  new ClienteNotFoundException();
        }
        throw (RuntimeException) exception;
    }
}
