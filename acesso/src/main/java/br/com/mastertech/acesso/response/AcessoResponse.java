package br.com.mastertech.acesso.response;

public class AcessoResponse {

    private long porta_id;
    private long Cliente_id;

    public long getPorta_id() {
        return porta_id;
    }

    public void setPorta_id(long porta_id) {
        this.porta_id = porta_id;
    }

    public long getCliente_id() {
        return Cliente_id;
    }

    public void setCliente_id(long cliente_id) {
        Cliente_id = cliente_id;
    }
}
