package br.com.mastertech.acesso.model;

import br.com.mastertech.acesso.request.AcessoRequest;
import br.com.mastertech.acesso.response.AcessoResponse;
import org.springframework.stereotype.Component;

@Component
public class AcessoMapper {

    public Acesso toAcesso(AcessoRequest request){
        Acesso acesso = new Acesso();
        acesso.setIdCliente(request.getCliente_id());
        acesso.setIdPorta(request.getPorta_id());
        return acesso;
    }

    public AcessoResponse toResponse(Acesso dadosAcesso){
        AcessoResponse response = new AcessoResponse();
        response.setCliente_id(dadosAcesso.getIdCliente());
        response.setPorta_id(dadosAcesso.getIdPorta());
        return response;
    }

}
