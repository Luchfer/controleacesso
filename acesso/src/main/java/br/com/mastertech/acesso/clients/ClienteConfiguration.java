package br.com.mastertech.acesso.clients;


import br.com.mastertech.acesso.clients.exception.ClienteBalanceFallBack;
import feign.Feign;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;


public class ClienteConfiguration {

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallbackFactory( ClienteBalanceFallBack::new, RuntimeException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }

}
