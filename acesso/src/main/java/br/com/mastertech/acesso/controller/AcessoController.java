package br.com.mastertech.acesso.controller;

import br.com.mastertech.acesso.exception.AcessoNotFoundException;
import br.com.mastertech.acesso.model.Acesso;
import br.com.mastertech.acesso.model.AcessoMapper;
import br.com.mastertech.acesso.request.AcessoRequest;
import br.com.mastertech.acesso.response.AcessoResponse;
import br.com.mastertech.acesso.service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/acesso")
public class AcessoController {
    @Autowired
    private AcessoService acessoService;

    @Autowired
    private AcessoMapper acessoMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AcessoResponse create(@RequestBody @Valid AcessoRequest request){
        Acesso acesso = acessoMapper.toAcesso(request);
        acesso =  acessoService.create(acesso);
        return acessoMapper.toResponse(acesso);
    }

    @GetMapping("/{clientId}/{portaId}")
    public AcessoResponse getAcess(@PathVariable long clientId,@PathVariable long portaId){
        Acesso acesso =  acessoService.getAcesso(clientId,portaId);
        if(acesso == null){
            throw new AcessoNotFoundException();
        }
        return acessoMapper.toResponse(acesso);
    }
}
