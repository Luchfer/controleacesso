package br.com.mastertech.acesso.repository;

import br.com.mastertech.acesso.model.Acesso;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AcessoRepository extends JpaRepository<Acesso,Long>{
    Acesso findByIdPortaAndIdCliente(long idPorta,long idCliente);
}
