package br.com.mastertech.acesso.request;

import javax.validation.constraints.NotNull;

public class AcessoRequest {
    @NotNull
    private long porta_id;
    @NotNull
    private long cliente_id;


    public long getPorta_id() {
        return porta_id;
    }

    public void setPorta_id(long porta_id) {
        this.porta_id = porta_id;
    }

    public long getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(long cliente_id) {
        this.cliente_id = cliente_id;
    }
}
