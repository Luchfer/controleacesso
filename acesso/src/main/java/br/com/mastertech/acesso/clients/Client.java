package br.com.mastertech.acesso.clients;

import br.com.mastertech.acesso.clients.dto.ClienteDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="cliente" , configuration = ClienteConfiguration.class)
public interface Client {

    @GetMapping("/cliente/{idCliente}")
    ClienteDTO getCliente(@PathVariable long idCliente);


}
