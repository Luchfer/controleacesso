package br.com.mastertech.acesso.clients;


import br.com.mastertech.acesso.clients.dto.PortaDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="porta")
public interface PortaClient {

    @GetMapping("porta/id/{idPorta}")
    PortaDTO getPorta(@PathVariable long idPorta);

}
