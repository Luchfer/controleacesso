package br.com.mastertech.logacesso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogacessoApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogacessoApplication.class, args);
	}

}
