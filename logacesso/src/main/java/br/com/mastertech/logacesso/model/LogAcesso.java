package br.com.mastertech.logacesso.model;

public class LogAcesso {

    public Boolean possuiAcesso;
    public long idCliente;
    public long idPorta;

    public Boolean getPossuiAcesso() {
        return possuiAcesso;
    }

    public void setPossuiAcesso(Boolean possuiAcesso) {
        this.possuiAcesso = possuiAcesso;
    }

    public long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(long idCliente) {
        this.idCliente = idCliente;
    }

    public long getIdPorta() {
        return idPorta;
    }

    public void setIdPorta(long idPorta) {
        this.idPorta = idPorta;
    }
}
