package br.com.mastertech.logacesso;

import br.com.mastertech.logacesso.model.LogAcesso;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class LogAcessoConsumer {

    @KafkaListener(topics = {"spec4-fernando-luchiari-1","spec4-fernando-luchiari-2","spec4-fernando-luchiari-3"}, groupId = "Luchiari")
    public void receber(@Payload LogAcesso acesso) {
        System.out.println("Tentativa de acesso: idPorta: " + acesso.getIdPorta()+" idCliente: " + acesso.getIdCliente()+" Possui acesso: " + acesso.getPossuiAcesso());
    }

}
