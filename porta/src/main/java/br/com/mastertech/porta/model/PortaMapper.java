package br.com.mastertech.porta.model;

import br.com.mastertech.porta.request.PortaRequest;
import br.com.mastertech.porta.response.PortaResponse;
import org.springframework.stereotype.Component;

@Component
public class PortaMapper {

    public Porta toPorta(PortaRequest request){
        Porta porta = new Porta();
        porta.setAndar(request.getAndar());
        porta.setSala(request.getSala());
        return porta;
    }

    public PortaResponse toResponse(Porta porta){
        PortaResponse response = new PortaResponse();
        response.setId(porta.getId());
        response.setAndar(porta.getAndar());
        response.setSala(porta.getSala());
        return response;
    }


}
