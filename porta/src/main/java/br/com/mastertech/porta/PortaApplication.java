package br.com.mastertech.porta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

@SpringBootApplication
@RibbonClient(
		name = "RibbonConfig",
		configuration = RibbonConfiguration.class)
public class PortaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PortaApplication.class, args);
	}

}
