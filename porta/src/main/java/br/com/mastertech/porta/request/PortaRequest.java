package br.com.mastertech.porta.request;

import javax.validation.constraints.NotBlank;

public class PortaRequest {

    private String andar;
    private String sala;

    @NotBlank
    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }
    @NotBlank
    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }
}
