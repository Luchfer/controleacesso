package br.com.mastertech.porta.controller;

import br.com.mastertech.porta.model.Porta;
import br.com.mastertech.porta.model.PortaMapper;
import br.com.mastertech.porta.request.PortaRequest;
import br.com.mastertech.porta.response.PortaResponse;
import br.com.mastertech.porta.service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping
public class PortaController {


    @Autowired
    private PortaService portaService;

    @Autowired
    private PortaMapper portaMapper;

    @PostMapping("/porta")
    @ResponseStatus(HttpStatus.CREATED)
    public PortaResponse createPorta(@RequestBody @Valid PortaRequest portaRequest){
        Porta porta = portaMapper.toPorta(portaRequest);
        porta = portaService.createPorta(porta);
        return portaMapper.toResponse(porta);
    }

    @GetMapping("/porta/id/{id}")
    public PortaResponse getPorta(@PathVariable(value="id") long id){
        Porta porta = portaService.getPorta(id);
        return portaMapper.toResponse(porta);
    }
}
